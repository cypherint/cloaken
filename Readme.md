# Cloaken AMI is available on the AWS marketplace
[Cloaken AWS](https://aws.amazon.com/marketplace/pp/B07RG8M2TN?qid=1566588861421&sr=0-1&ref_=srh_res_product_title)  

# Ami launch instructions

1. choose the instance type for your use case
2. configure your instance
3. add storage:  choose the size that will fit your usage.  The
database holds the urls that are detonated.
4. add tags
5. configure security group:  
    a.  add a rule to allow  tcp port 443 and tcp port 80 ; 
    source should be only your public ip 
    you will be sending requests from; description webserver
6. create a new key pair. download key.    

## Register a dns 
1. using the ip address from the aws console for the created ec2 register for a 
valid dns.  keep this for later.
    
## Run installation
1. cd /home/ubuntu/unshorten/utilites

2. ./initialize.sh 

# Api 
##  Login  
1.  https://[yourserver]/api/authlogin

# Gui
## Login
1.  https://[yourserver/

# Manual Install
## System dependencies
1. postgresql
1. chromium
1. chromewebdriver

## Manual Installation  
1. install dependencies
    1. posgresql
    1. chromium
    1. chromewebdriver
        1. [instructions](https://chromedriver.chromium.org/getting-started)
        1. ensure chromewebdriver is in the PATH
        1. ensure versions match on chromewebdriver and chrome/chromium

1. ssh login:
    ```
    ssh -i [downloaded pemfile] ubuntu@[your ec2dns]
    ```

2. from /home/ubuntu/unshorten run
    ```
    cd ~/unshorten
    pipenv shell
    python manage.py generate_secret_key --replace
    python manage.py createsuperuser
    ```

3. restart gunicorn 
    ```
    sudo systemctl daemon-reload
    sudo systemctl restart gunicorn
    ```
    
4. create a public dns on your preferred dns provider or use the aws dns from
   the console. noip.com gives free dns.
    
   [reference instructions](https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-16-04)
    
5. `sudo nano /etc/nginx/sites-available/default`
    
   replace the second underscore here with your dns name  `server_name _` 
    
6. `sudo systemctl restart nginx`

7. run certbot ( you may have to wait about 10 minutes for the dns to propogate)
    
    `sudo certbot --nginx -d [your registered dns name]`
    
    a.  choose 2 to redirect port 80 to 443
    
 
8. you should see success:
    ``` 
    Output
    Please choose whether or not to redirect HTTP traffic to HTTPS, removing HTTP access.
    -------------------------------------------------------------------------------
    1: No redirect - Make no further changes to the webserver configuration.
    2: Redirect - Make all requests redirect to secure HTTPS access. Choose this for
    new sites, or if you're confident your site works on HTTPS. You can undo this
    change by editing your web server's configuration.
    -------------------------------------------------------------------------------
    Select the appropriate number [1-2] then [enter] (press 'c' to cancel):
    ``` 
9. test dry run of renewal

    `sudo certbot renew --dry-run`     

# SDK

CloakenSDK is the preferred SDK to interact with Cloaken.  
Usage and instructions can be found on the gitlab page   
[Cloaken SDK](https://gitlab.com/cypherint/cloakensdk)  

# Troubleshooting 
 
1. restart services:

    a.  sudo systemctl restart nginx  
    b.  sudo systemctl restart gunicorn  
    c.  sudo systemctl restart cloakenjob  
    
# AMI ADeveloper Code Deployment commands  
1. `cd ~/unshorten`
1. `git pull`  

2. `cd ~/unshorten/frontend`  
3. `npm install`  
3. `npm run build`  

4. `cd ~/unshorten
5. `pipenv install`
6. `pipenv run manage.py migrate`

# AMI Upgrade from v0.1 to v2
1. `cd ~/unshorten/`  
3. `git remote set-url origin git@gitlab.com:cypherint/cloaken.git` 
2. `git pull`  

3. `pipenv install`
4. `pipenv shell`

3. `python manage.py migrate`

4. `cd ~/unshorten/frontend`
5. `npm install`
3. `npm run build`  

4. `mkdir ~/unshorten/media` 
5. add to /etc/nginx/sites-available/default
`local /media/ {
root /home/ubuntu/unshorten;
}`

5.  add unit file for job service
```
[Unit]
Description=cloakenjob service
After=network.target

[Service]
User=ubuntu
Group=www-data
WorkingDirectory=/home/ubuntu/unshorten/
Environment="PATH=/home/ubuntu/.venvs/unshorten-49qmfdSq/lib/python3.6/site-packages/:/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin"
ExecStart=/home/ubuntu/.venvs/unshorten-49qmfdSq/bin/python manage.py process_tasks

[Install]
WantedBy=multi-user.target
`
6. `sudo systemctl enable cloakenjob`
7.  reboot
```
