import React from 'react';
import ReactDOM from 'react-dom';
import { Route,Switch } from 'react-router-dom'
import { Provider } from 'react-redux'

import { isAuthenticated} from './reducers'
import Login from './containers/Login';
import PrivateRoute from './containers/PrivateRoute';
import Detonate from './components/Detonate'
import About from './components/About'
import Logout from './components/Logout'


//todo(aj) put nested routes inside Detonate under the navigation
function App(props) {
  return (
    <Switch>
      <Route exact path="/login/" component={Login} />
      <PrivateRoute path="/" component={Detonate} />
      
    </Switch>
    )
}


export default App
