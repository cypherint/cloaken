import { RSAA } from 'redux-api-middleware';
import { withAuth } from '../reducers'
import uuid from 'uuid';

export const LOADING_START = "START_LOADING"
export const LOADING_FINISHED = "FINISHED_LOADING"

export const startLoading = (type,id='')=>({
  type:LOADING_START,
  payload:{type,id}
})

export const finishLoading = (type,id='')=>({
  type:LOADING_FINISHED,
  payload:{type,id}
})

export const cancel = ()=>({
  type:CANCEL,
  payload:null
  
})
