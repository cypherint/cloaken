import { RSAA } from 'redux-api-middleware';
import { withAuth } from '../reducers'
import {unshorten,cancel} from '../actions/unshorten'
export const RASTERIZE_DONOTHING="RASTERIZE/DONOTHING"
export const POLLING_SUCCESS="POLLING/SUCCESS"
export const POLLING_FAILURE="POLLING/FAILED" //todo(aj)
export const POLLING_CANCEL="POLLING/CANCEL" //not needed
export const POLLING_TIMEOUT="POLLING/CANCEL" //todo(aj)

export const doNothing= (url)=>(
  {type:RASTERIZE_DONOTHING,
    payload:{org_url:url}
  }
)
export const pollingCancel= (url)=>(
  {type:POLLING_CANCEL,
    payload:{org_url:url}
  }
)
export const pollingFailure = (org_url, hash) =>(
  {
    type:POLLING_FAILURE,
    payload:{
      org_url:org_url,
      task_hash:hash
    },
  }
)
export const pollingTimeout = (org_url, hash) =>(
  {
    type:POLLING_TIMEOUT,
    payload:{
      org_url:org_url,
      task_hash:hash
    }

  }
)
export const pollingSuccess = (org_url,hash,status)=>({
    type: POLLING_SUCCESS,
    payload:{task_hash:hash,
              org_url:org_url,
              task_status:status}
})


export const TASK_REQUEST = '@@unshorten/TASK_REQUEST';
export const TASK_SUCCESS = '@@unshorten/TASK_SUCCESS';
export const TASK_FAILURE = '@@unshorten/TASK_FAILURE';
export const CHECK_TASK_POLL = "@@unshorten/CHECK_TASK_POLL"

export const checkTaskPoll = (hash,org_url) =>({
  type:CHECK_TASK_POLL,
  payload:{task_hash:hash,org_url:org_url}
})

// called from checkTaskPoll
export const checkTask = (hash,org_url) => ({
 [RSAA]: {
    endpoint: '/api/task_status/'+hash,
      method: 'GET',
      headers: withAuth({ 'Content-Type': 'application/json' }),
      types: [
        {
          type:TASK_REQUEST, 
          meta:{hash:hash,org_url:org_url}
        },
        {
          type:TASK_SUCCESS, 
          meta:{hash:hash,org_url:org_url}
        },
        {
          type:TASK_FAILURE, 
          meta:{hash:hash,org_url:org_url},
        },
      ]
  }

})

export function asyncActionDispatch(url){
    return async(dispatch, getState)=>{
      const response = await dispatch(rasterize(url))
      let i = 1
      if (response.error){
        //failure handled in saga
        return await dispatch(doNothing(url))
      }
      //wrap dispatch in promise so it is async
      //launch checkTaskPoll which signlas saga to launch 
      return  await dispatch(checkTaskPoll(response.payload.task_hash,response.meta.org_url))
    }
 
}

//unshorten and then rasterize
export function submitRasterize(url){

  return async(dispatch, getState)=>{
    const response = await dispatch(unshorten(url))
    if(response.error){
      // break out
      console.log("errors"+response.error)
      return await dispatch(pollingCancel(url))
    }

    return await dispatch(asyncActionDispatch(url))
  }
}

export const RASTERIZE_REQUEST = '@@unshorten/RASTERIZE_REQUEST';
export const RASTERIZE_SUCCESS = '@@unshorten/RASTERIZE_SUCCESS';
export const RASTERIZE_FAILURE = '@@unshorten/RASTERIZE_FAILURE';
export const RASTERIZE_DELETE = '@@unshorten/RASTERIZE_DELETE';

export const deleteRasterize = (url) =>({
    type:RASTERIZE_DELETE,
    payload:{org_url:url}
})
export const rasterize = (url) => ({
  [RSAA]: {
    endpoint: '/api/urls/rasterize/',
      method: 'POST',
      body: JSON.stringify({url: url}),
      headers: withAuth({ 'Content-Type': 'application/json' }),
      types: [
        {
          type:RASTERIZE_REQUEST, 
          meta:{org_url:url}
        },
        {
          type:RASTERIZE_SUCCESS, 
          meta:{org_url:url}
        },
        {
          type:RASTERIZE_FAILURE, 
          meta:{org_url:url},
        },
      ]
  }
})


