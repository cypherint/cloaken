import { RSAA } from 'redux-api-middleware';
import { withAuth } from '../reducers'
import  URL  from  'url-parse'
import { getImage} from '../util'



export const RASTERIZERESULTS_REQUEST = '@@rasterizerequest/RASTERIZERESULTS_REQUEST';
export const RASTERIZERESULTS_SUCCESS = '@@rasterizerequest/RASTERIZERESULTS_SUCCESS';
export const RASTERIZERESULTS_FAILURE = '@@rasterizerequest/RASTERIZERESULTS_FAILURE';

// called from checkTaskPoll
export const getRasterizeResult= (hash) => ({
 [RSAA]: {
    endpoint: '/api/rasterizeresults/?task_hash='+hash,
      method: 'GET',
      headers: withAuth({ 'Content-Type': 'application/json' }),
      types: [
        {
          type:RASTERIZERESULTS_REQUEST,
          meta:{hash:hash}
        },
        {
          type:RASTERIZERESULTS_SUCCESS, 
          meta:{hash:hash}
        },
        {
          type:RASTERIZERESULTS_FAILURE, 
          meta:{hash:hash},
        },
      ]
  }

})

export const RASTERIZEHASH_HIDE= '@@rasterizerequest/RASTERIZEHASH_HIDE';
export const hideRasterizeFile= (hash)=>{
  return {
    type:RASTERIZEHASH_HIDE,
    payload:hash
  }
}

export const RASTERIZEHASH_REQUEST = '@@rasterizerequest/RASTERIZEHASH_REQUEST';
export const RASTERIZEHASH_SUCCESS = '@@rasterizerequest/RASTERIZEHASH_SUCCESS';
export const RASTERIZEHASH_FAILURE = '@@rasterizerequest/RASTERIZEHASH_FAILURE';
function readAsBase64(blob) {
  return new Promise((resolve, reject) => {
    // const reader = new FileReader();
    // callback on load
    //reader.onload = () => {
    //  const base64data = reader.result;
    //  resolve(base64data);
    //}
    //callback on error
    //reader.onerror = (err) => {
    //  reject(err);
    //}
    //reader.readAsDataURL(blob.slice(0,blob.size,"image/png")); 
    const localImageUrl = window.URL.createObjectURL(blob.slice(0,blob.size,"image/png"))
    resolve(localImageUrl)
    reject()
  });
}
// called from checkTaskPoll
export const getRasterizeFile= (url,hash) => {
  let fullUrl = new URL(url)
  let path = fullUrl.pathname
  return {
 [RSAA]: {
    endpoint: path,
      method: 'GET',
      headers: withAuth({ 'Content-Type': 'application/json' }),
      types: [
        {
          type:RASTERIZEHASH_REQUEST,
          meta:{hash:hash}
        },
        {
          type:RASTERIZEHASH_SUCCESS, 
          meta:{hash:hash},
          payload:(action, state, res) => getImage(res).then(readAsBase64)
        },
         
        {
          type:RASTERIZEHASH_FAILURE, 
          meta:{hash:hash}
        },
      ]
  }

  }
}
export const RESULTHASH_CANCEL = 'rasterizerequest/RASTERIZEHASH_CANCEL'

export const rasterizeCancel = ()=>({
  type:RESULTHASH_CANCEL
})

export const getRasterizeResultFile = (hash) => {

  return async(dispatch, getState) =>{
    const response = await dispatch(getRasterizeResult(hash))
    if(response.error){
      // break out
      console.log("errors"+response.error)
      return await dispatch(rasterizeCancel())
    }

    return await dispatch(getRasterizeFile(response.payload.results[0].page_png,hash))

  }

}
