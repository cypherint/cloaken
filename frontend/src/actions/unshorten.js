import { RSAA } from 'redux-api-middleware';
import { withAuth } from '../reducers'
import uuid from 'uuid';

export const UNSHORTEN_REQUEST = '@@unshorten/UNSHORTEN_REQUEST';
export const UNSHORTEN_SUCCESS = '@@unshorten/UNSHORTEN_SUCCESS';
export const UNSHORTEN_FAILURE = '@@unshorten/UNSHORTEN_FAILURE';
export const CANCEL = "UNSHORTEN/CANCEL"

export const unshorten = (url) => ({
  [RSAA]: {
    endpoint: '/api/urls/unshorten/',
      method: 'POST',
      body: JSON.stringify({url: url}),
      headers: withAuth({ 'Content-Type': 'application/json' }),
      types: [
        {
          type:UNSHORTEN_REQUEST, 
          meta:{org_url:url}
        },
        {
          type:UNSHORTEN_SUCCESS, 
          meta:{org_url:url}
        },
        {
          type:UNSHORTEN_FAILURE, 
          meta:{org_url:url}
        },
      ]
  }
})

export const LOADING_START = "START_LOADING"
export const LOADING_FINISHED = "FINISHED_LOADING"

export const startLoading = (type,id='')=>({
  type:LOADING_START,
  payload:{type,id}
})

export const finishLoading = (type,id='')=>({
  type:LOADING_FINISHED,
  payload:{type,id}
})

export const cancel = ()=>({
  type:CANCEL,
  payload:null
  
})
