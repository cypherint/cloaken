import React from 'react';
import ReactDOM from 'react-dom';

const About = (props)=>(
  <div>
    <div className="row">
      <div className="col-sm-4 offset-sm-4" >
      <h1>About</h1>
      <hr/>
      <h2>Cloaken</h2>
      <p>
        Version 2<br/>
        support email: support@cypherint.com
      </p>
      </div>
    </div>
  </div>
)

export default About 

