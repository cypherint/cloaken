import React from 'react'
import { Col,Alert,Form,FormGroup,Button,ListGroup,ListGroupItem } from 'reactstrap';
import {LOADING} from '../reducers/unshorten'
import ShowHideImage from './showHideImage'
import propTypes from 'prop-types'
export default class Main extends React.Component{

  constructor(props){
  super(props)

  }
  render(){
    return (
  <ListGroup>
        {this.props.order.map((key,index) =>
          <ListGroupItem key={key}>
            <ListGroup>
              <ListGroupItem>
              Original: {this.props.urls[key].url}
              </ListGroupItem>
              <ListGroupItem>
                {this.props.urls[key].unshortened_url===LOADING ?
                     <span className="spinner-border" role="status">
                        <span className="sr-only">Loading...</span>
                      </span>:this.props.urls[key].unshortened_url
                    } 
              </ListGroupItem>

                { key in this.props.rasterizeUrls?
                <ShowHideImage
                  orgUrl={key}
                  showPhoto={this.props.showPhoto}
                  hidePhoto={this.props.hidePhoto}
                  unshortenUrl={this.props.urls[key].unshortened_url}
                  rasterizeUrls={this.props.rasterizeUrls}
                  rasterizeHashes={this.props.rasterizeHashes}>
                </ShowHideImage>:null
              }
            </ListGroup>
          </ListGroupItem>)}
      </ListGroup>
 
    ) 
  }
}

Main.propTypes = {
  order:propTypes.array.isRequired,
  showPhoto:propTypes.func.isRequired,
  hidePhoto:propTypes.func.isRequired,
  urls:propTypes.array.isRequired,
  rasterizeUrls:propTypes.array.isRequired,
  rasterizeHashes:propTypes.array.isRequired

};
