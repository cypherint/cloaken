import React from 'react'
import {SUCCEEDED,RUNNING} from '../saga/pollSnapshot'
import { Col,Alert,Form,FormGroup,Button,ListGroup,ListGroupItem } from 'reactstrap';
import {LOADING} from '../reducers/unshorten'
//todo(aj) add string prop for hide photo showPhoto function will handle switchinggt
export default ({orgUrl,
  showPhoto,
  hidePhoto,
  unshortenUrl,
  rasterizeUrls,
  rasterizeHashes,
  ...rest}) =>{
  // rasterize response received and succeeded 
  if (orgUrl in rasterizeUrls && 
    rasterizeUrls[orgUrl].rasterizeStatus===SUCCEEDED &&  
    !(rasterizeUrls[orgUrl].hash in rasterizeHashes) 
       ){    //first time or not showing Photo
    return (
     <ListGroupItem>
      <Button className="button-brand-primary" size="lg" value={rasterizeUrls[orgUrl].hash}
        onClick={showPhoto}>Photo</Button>
    </ListGroupItem>
        )
  }

  // showing HidePhotoButton 
  if (orgUrl in rasterizeUrls && 
    rasterizeUrls[orgUrl].rasterizeStatus===SUCCEEDED &&  //todo(aj) I think this line can go
    (rasterizeUrls[orgUrl].hash in rasterizeHashes) 
    ){
    // show hidebutton and image
    if(rasterizeHashes[rasterizeUrls[orgUrl].hash].file != null){
      return (
        <div>
          <ListGroupItem>
            <Button className="button-brand-primary" size="lg" value={rasterizeUrls[orgUrl].hash}
              onClick={hidePhoto}>Hide Photo</Button>
          </ListGroupItem>
            <ListGroupItem>
            <img className="img-fluid" src={rasterizeHashes[rasterizeUrls[orgUrl].hash].file}/>
          </ListGroupItem>
        </div>
      )
   
    }
    else{
      //else just show loader; wiating on image download
      return (
       <ListGroupItem>
          <span className="spinner-border" role="status">
            <span className="sr-only">Loading...</span>
          </span>
       </ListGroupItem>
      ) 
    }
  }

  if (orgUrl in rasterizeUrls){
    return (
     <ListGroupItem>
          <span className="spinner-border" role="status">
            <span className="sr-only">Loading...</span>
            </span>
          </ListGroupItem>
    )
  }

}

