import React from 'react'
import ReactDOM from 'react-dom';
import {unshorten,cancel} from '../actions/unshorten'
import {rasterize, submitRasterize, deleteRasterize, 
  checkTaskPoll,asyncActionDispatch} from '../actions/rasterize'
import {getRasterizeResultFile,hideRasterizeFile} from '../actions/rasterizeHashes'
import {startPollingSaga} from '../saga/pollSnapshot'
import { connect } from 'react-redux'
import * as reducers from '../reducers/'
import TextInput from '../components/TextInput'
import SubmitButton from '../components/Submit.js'
import {LOADING} from '../reducers/unshorten'
import OutputList from '../components/outputlist'
import { take, put, delay } from 'redux-saga/effects'


import { Col,Alert,Form,FormGroup,Button,ListGroup,ListGroupItem } from 'reactstrap';
class Main extends React.Component{

  constructor(props){
    super(props)
    this.state={
      url:'',
    }
    this.handleClick = this.handleClick.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleShowPhoto = this.handleShowPhoto.bind(this)
    this.handleHidePhoto = this.handleHidePhoto.bind(this)
  }

  handleChange(event){
    this.setState({
      url:event.target.value
    })
  }
  //todo(aj) hide photo pass into outputlist
  //outputList will contain all the logic for show string/hidestring and showfunc/hidefunc
  handleHidePhoto(event){
    event.preventDefault()
    this.props.hidePhoto(event.target.value)
  }
  handleShowPhoto(event){
    //todo(aj) show if hidden/ hide if showing
    event.preventDefault()
    this.props.showPhoto(event.target.value)
  }
  handleClick(event){
    //call action to unshorten url
    event.preventDefault()
    if(this.props.submitLoading){
      this.props.onCancel()}
    else{
      this.props.onSubmit(this.state.url)  //Todo(aj) shit just raterize here then rasterize can just show a loader until ready
    }
  }

  render(){
    
    const errors = this.props.errors || {}
    const rasterizeHashErrors = this.props.rasterizeHashErrors || {}
    const rasterizeErrors = this.props.rasterizeErrors || {}

    return(
     <div className="col-sm-8 offset-sm-2" >
        <Form onSubmit={this.handleClick}>
            {errors.non_field_errors?<Alert color="danger">{errors.non_field_errors}</Alert>:""}
            {rasterizeHashErrors.non_field_errors?<Alert color="danger">{rasterizeHashErrors.non_field_errors}</Alert>:""}
            {rasterizeErrors.non_field_errors?<Alert color="danger">{rasterizeErrors.non_field_errors}</Alert>:""}

            <TextInput   
              label="URL"
              error={errors.url}
              name="urlinput"  
              value = {this.state.url} onChange={this.handleChange}/>
            <SubmitButton />          
     </Form>
     
     <OutputList 
       order={this.props.order} 
       urls={this.props.urls} 
       rasterizeUrls={this.props.rasterizeUrls}
       rasterizeHashes={this.props.rasterizeHashes}
       showPhoto={this.handleShowPhoto} 
       hidePhoto={this.handleHidePhoto}
     ></OutputList>

    </div>
  
    )
 
 }
}

const mapStateToProps = (state) => ({
  urls:reducers.get_urls(state),
  errors:reducers.urlErrors(state),
  rasterizeErrors:reducers.rasterizeErrors(state),
  rasterizeHashErrors:reducers.rasterizeHashErrors(state),
  submitLoading:reducers.submitLoading(state),
  order:reducers.getOrder(state),
  rasterizeUrls:reducers.getRasterizeUrls(state),
  rasterizeHashes:reducers.getRasterizeHashes(state)
})

//todo(aj) hidePhotoAction
const mapDispatchToProps = (dispatch) => ({
  onRasterize: (url) =>{
    dispatch(asyncActionDispatch(url))
 },
  onSubmit: (url) => {
    dispatch(submitRasterize(url))
  },
  onCancel:()=>{
    dispatch(cancel())
  },
  deleteRasterize:(url)=>{
    dispatch(deleteRasterize(url))
  },
  showPhoto:(hash)=>{
    dispatch(getRasterizeResultFile(hash))
  },
  hidePhoto:(hash)=>{
    dispatch(hideRasterizeFile(hash))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Main);
