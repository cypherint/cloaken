import React from 'react';
import ReactDOM from 'react-dom';
import createHistory from 'history/createBrowserHistory'
import { BrowserRouter ,Route,Switch } from 'react-router-dom'
import { Provider } from 'react-redux'

import 'bootstrap/dist/css/bootstrap.css';
import './custom.css';
import Login from './containers/Login';
import configureStore from './store'
import Detonate from './components/Detonate'
import App from './App'
import About from './components/About'
import Logout from './components/Logout'
import {PersistGate} from 'redux-persist/integration/react'
import {root} from './saga/pollSnapshot.js'

const history = createHistory()

const {store,persistor,sagaMiddleware} = configureStore(history)

ReactDOM.render((
  <Provider store={store}>
    <PersistGate loading={null} persistor = {persistor}>
      <BrowserRouter basename="cloaken">
        <App/>
      </BrowserRouter>
    </PersistGate>
  </Provider>
), document.getElementById('root'));

sagaMiddleware.run(root)
