import { combineReducers } from 'redux'
import auth, * as fromAuth from './auth.js'
import echo, * as fromEcho from './echo.js'
import unshorten, * as fromUnshorten from './unshorten.js'
import password, * as fromPassword from './password.js'
import rasterize, * as fromRasterize from './rasterize.js'
import rasterizeHashes, * as fromRasterizeHashes from './rasterizeHashes'
export default combineReducers({
  unshorten: unshorten,
  auth: auth,
  password:password,
  rasterizeUrls:rasterize,
  rasterizeHashes:rasterizeHashes
})

export const serverMessage = state => fromEcho.serverMessage(state.echo)
//fromAuth
export const isAuthenticated = state => fromAuth.isAuthenticated(state.auth)
export const get_username = state => fromAuth.get_username(state.auth)
export const accessToken = state => fromAuth.accessToken(state.auth)
export const isAccessTokenExpired = state => fromAuth.isAccessTokenExpired(state.auth)
export const refreshToken = state => fromAuth.refreshToken(state.auth)
export const isRefreshTokenExpired = state => fromAuth.isRefreshTokenExpired(state.auth)
export const authErrors = state => fromAuth.errors(state.auth)

//fromPassword
export const getPasswordChanged = state => fromPassword.getPasswordChanged(state.password)

//fromUnshorten
export const unshortenerrors = state => fromUnshorten.errors(state.unshorten)
export const get_urls = state => fromUnshorten.get_urls(state.unshorten)
export const submitLoading = state =>fromUnshorten.submitLoading(state.unshorten)
export const urlErrors = state =>fromUnshorten.errors(state.unshorten)
export const getOrder = state =>fromUnshorten.getOrder(state.unshorten)

//fromRasterize
export const getRasterizeUrls = state => fromRasterize.getUrls(state.rasterizeUrls)
export const rasterizeErrors= state =>fromRasterize.errors(state.rasterizeUrls)


//fromRasterizeResults
export const getRasterizeHashes = state => fromRasterizeHashes.getHashes(state.rasterizeHashes)
export const rasterizeHashErrors= state =>fromRasterizeHashes.errors(state.rasterizeHashes)

export function withAuth(headers={}) {
  return (state) => ({
    ...headers,
    'Authorization': `Bearer ${accessToken(state)}`
  })
}
