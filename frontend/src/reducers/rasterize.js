// initial state

import * as  rasterize from '../actions/rasterize'
const initialState = {
  urls:{},
  errors:{}
}

function UrlInfo(
  url,
  hash,
  rasterizeStatus,
  rasterizeLoading,
  ){
  this.url=url;
  this.hash=hash;
  this.rasterizeStatus=rasterizeStatus;
  this.rasterizeLoading=rasterizeLoading;
}

function UrlInfo_set_url(urlinfo,url){
  let new_urlinfo = {...urlinfo,
              url:url
              }
  return new_urlinfo
}

function UrlInfo_set_hash(urlinfo,hash){
  let new_urlinfo = {...urlinfo,
              hash:hash
              }
  return new_urlinfo
}

function UrlInfo_set_rasterizeStatus(urlinfo,rasterizeStatus){
  let new_urlinfo = {...urlinfo,
              rasterizeStatus:rasterizeStatus
              }
  return new_urlinfo
}

function UrlInfo_set_rasterizeLoading(urlinfo,rasterizeLoading){
  let new_urlinfo = {...urlinfo,
              rasterizeLoading:rasterizeLoading
              }
  return new_urlinfo
}

export default (state= initialState, action) => {
  const type=null;
  const id=null;
  switch(action.type) {
    //rasterize request
    case rasterize.RASTERIZE_DELETE:
      {
      let new_urls_deleted = Object.assign(state.urls,{})
      if (action.payload.org_url in new_urls_deleted){
        delete new_urls_deleted[action.payload.org_url]
      }
      return {
        errors:{},
        urls:{...new_urls_deleted}
      }
     }
    case rasterize.RASTERIZE_FAILURE:
      {
      let new_urls_deleted = Object.assign(state.urls,{})
      if (action.payload.org_url in new_urls_deleted){
        delete new_urls_deleted[action.payload.org_url]
      }
 
     return {
        errors:{'non_field_errors':action.payload.response || action.payload.message},
        urls:{
        ...new_urls_deleted,
        },
      }
    }
    case rasterize.RASTERIZE_REQUEST:
      {
      let new_url = new UrlInfo(action.meta.org_url,
                                null,
                                null,
                                true,{})
      return {
        errors:{},
        urls:{
          ...state.urls,
          [action.meta.org_url]:new_url
          }
      }
     }
    //returns hash for polling
    case rasterize.RASTERIZE_SUCCESS:
      {
      let url_update_success = Object.assign(state.urls[action.meta.org_url],{})
      url_update_success = UrlInfo_set_hash(url_update_success,action.payload.task_hash)
      return {
        errors:{},
        urls:{
            ...state.urls,
            [action.meta.org_url]:url_update_success
        }
      }
    }
    // todo POLLING_CANCEL   // canceled by user not implemented
    //raterization is complete returned success; change button to display picture
    case rasterize.POLLING_SUCCESS:
      {
      let url_update = Object.assign(state.urls[action.payload.org_url],{})
      url_update =  UrlInfo_set_rasterizeLoading(url_update,false)
      url_update =  UrlInfo_set_rasterizeStatus(url_update,action.payload.task_status)
      return {
        errors:{},
        urls:{
          ...state.urls,
          [action.payload.org_url]:url_update
        }
      }
     }
    case rasterize.POLLING_TIMEOUT:
    case rasterize.POLLING_FAILURE:
      {
      let url_poll_fail= Object.assign(state.urls,{})
      if (action.payload.org_url in url_poll_fail){
        delete url_poll_fail[action.payload.org_url]
      }
      return {
        errors:{'non_field_errors':'Failed to Rasterize'},
        urls:{
          ...url_poll_fail,
        }
      }
    } 
    default:
       return state

  }
}

export function getUrls(state){
  return state.urls
}
export function errors(state){
  return state.errors
}
