// initial state

import * as  rasterizeHashes from '../actions/rasterizeHashes'
const initialState = {
  hashMap:{},
  errors:{}
}


function hashInfo(hash, file, downloadUrl,status){
  this.hash=hash
  this.file=file
  this.downloadUrl=downloadUrl
  this.status=status
}

function hashInfo_set_hash(hashInfo, hash){
  let new_hashInfo =  Object.assign(hashInfo,{})
  new_hashInfo.hash=hash
  return new_hashInfo
} 
function hashInfo_set_file(hashInfo, file){
  let new_hashInfo =  Object.assign(hashInfo,{})
  new_hashInfo.file=file
  return new_hashInfo
}
function hashInfo_set_download(hashInfo, download){
  let new_hashInfo =  Object.assign(hashInfo,{})
  new_hashInfo.downloadUrl=download
  return new_hashInfo
}
export const  TASK_HASH_LOADING="LOADING"
export const  TASK_HASH_FAILED="FAILED"
export const  TASK_HASH_OK="OK"

export default (state= initialState, action) => {
  const type=null;
  const id=null;
  switch(action.type) {
    case(rasterizeHashes.RASTERIZERESULTS_REQUEST):
      {
        let newUrl = new hashInfo(action.meta.hash,null, null, TASK_HASH_LOADING)
        return {
          hashMap:{
            ...state.hashMap,
            [action.meta.hash]:newUrl,
          },
          errors:{}
        }
     }
    case(rasterizeHashes.RASTERIZERESULTS_SUCCESS):
      {
        let newUrl = new hashInfo(action.payload.results[0].task_hash,
          null, 
          action.payload.results[0].page_png, 
          TASK_HASH_OK)
        return {
          hashMap:{
            ...state.hashMap,
            [action.payload.results[0].task_hash]:newUrl,
          },
          errors:{}
        }
      }
    case(rasterizeHashes.RASTERIZEHASH_SUCCESS):
      {
        let update = state.hashMap[action.meta.hash] 
        hashInfo_set_file(update,action.payload)
        return {
          hashMap:{...state.hashMap,
          [action.meta.hash]:update,
          },
          errors:{}
        }

      }
    case(rasterizeHashes.RASTERIZEHASH_HIDE):
      {
        let new_hashmap = Object.assign(state.hashMap,{})
        delete new_hashmap[action.payload]
        return {
          hashMap:{
            ...new_hashmap
          },
          errors:{}
        }
      }

    case(rasterizeHashes.RASTERIZERESULTS_FAILURE):
    case(rasterizeHashes.RASTERIZEHASH_FAILURE):
      {
        let new_hashmap= Object.assign(state.hashMap,{}) 
        delete new_hashmap[action.meta.hash]
        return {
          hashMap:{
            ...new_hashmap
          },
          errors:{'non_field_errors':action.payload.response || action.payload.message}
        }
      }
    default:
      return state
  }
}

export function getHashes(state){
  return state.hashMap
}

export function errors(state){
  return state.errors
}
