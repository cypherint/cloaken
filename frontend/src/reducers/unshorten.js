import * as  unshorten from '../actions/unshorten'
export const LOADING="Loading..."

const initialState = {
  urls:{},
  order:[],
  errors:{},
  submitLoading:false
}


let copy_state=(original) =>{
  let new_state = Object.assign({},original);
  return new_state;
}

export default (state=initialState, action) => {
  const type=null;
  const id=null;
  switch(action.type) {
    case unshorten.UNSHORTEN_FAILURE:
      let error_message=action.payload.response.url || action.payload.message
      let response_urls = {}
      response_urls = {
                        ...state.urls,
                        [action.meta.org_url]:{
                            url:action.meta.org_url,
                            unshortened_url:error_message,
                            submitLoading:false},
                    }

      return {
        urls:{...response_urls},
        errors: action.payload.response || {'non_field_errors': action.payload.message},
        order:Object.assign([],state.order),
        submitLoading:false
      }
    case unshorten.UNSHORTEN_REQUEST:
      let new_state = Object.assign({},state)
      var index = new_state.order.indexOf(action.meta.org_url);
      if (index > -1) {
          new_state.order.splice(index, 1);
      }
      new_state.order.splice(0,0,action.meta.org_url)
      return {
        urls:{
          ...new_state.urls,
          [action.meta.org_url]:{
          url:action.meta.org_url,
          submitLoading:true,
          unshortened_url:LOADING},
        },
        order:Object.assign([],state.order)
      }
    case unshorten.UNSHORTEN_SUCCESS:
      
     return {
        urls:{
          ...state.urls,
          [action.meta.org_url]:{
          url:action.meta.org_url,
          submitLoading:false,
          unshortened_url:action.payload.unshortened_url},
        },
        errors:{},
       order:Object.assign([],state.order),
       submitLoading:false
      }
    default:
      return state
  }
}
export function errors(state){
  return state.errors
}
export function get_urls(state){
  return state.urls
}
export function submitLoading(state){
  return state.submitLoading
}
export function getOrder(state){
  return state.order;
}
