import { takeEvery, race, take, put, call, delay } from 'redux-saga/effects'
import {checkTask,pollingFailure,pollingTimeout,CHECK_TASK_POLL,TASK_FAILURE,TASK_SUCCESS} from '../actions/rasterize.js'
import {POLLING_SUCCESS,
  POLLING_TIMEOUT,
  POLLING_FAILURE,
  POLLING_CANCEL,
  pollingSuccess} from '../actions/rasterize.js'

export const SUCCEEDED="completed"
export const RUNNING="running"
export const TIMEOUT=260000
export function* checkJobStatus(action) {
  let jobSucceeded = false;
  while (!jobSucceeded) {
    yield put(checkTask(action.payload.task_hash,action.payload.org_url));
    const pollingAction = yield take(TASK_SUCCESS);
    const pollingStatus = pollingAction.payload.task_status;
    switch (pollingStatus) {
      case SUCCEEDED:
        jobSucceeded = true;
        yield put(pollingSuccess(action.payload.org_url,
          action.payload.task_hash,
          pollingStatus));
        break;
      default:
        break;
    }
    // delay the next polling request in 5 second
    yield delay(5000);
  }
}

export function* startPollingSaga(action) {
    // Race the following commands with a timeout of 1 minute
    const { response, failed, timeout } = yield race({
      response: call(checkJobStatus,action),
      cancel: take(POLLING_CANCEL), //todo not activated yet; button could cancel request; not necessary ; could just leave timeout
      failed: take(TASK_FAILURE), // will return from RIAA
      timeout: delay(TIMEOUT)
    });
  // seems to be giving me trouble to take the task failure
   if (failed) {
     yield put(pollingFailure(action.meta.org_url,action.meta.task_hash)); //todo need to activate
   }
   if (timeout) {
     yield put(pollingTimeout(action.payload.org_url,action.payload.task_hash)); //todo need to activate
   }

}

// create action with payload.hash

export function* root(){
  //checkTaskPoll
    yield takeEvery(CHECK_TASK_POLL, startPollingSaga);
}
