const StreamToBlob = require('stream-to-blob')

export async function getImage(res) {
  const contentType = res.headers.get('Content-Type');
  const emptyCodes = [204, 205];

  if (
    !~emptyCodes.indexOf(res.status) &&
    contentType &&
    ~contentType.indexOf('image/png')
  ) {
    return res.blob()
  } else {
    return await Promise.reject();
  }
}
