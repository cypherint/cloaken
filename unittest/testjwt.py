"""
live integration tests
set 2 environment variables
username
password
"""

import requests
import os
from datetime import datetime
import unittest


class JWT(object):
    GET="GET"
    POST="POST"
    JSON_FORMAT="application/json"
    REFRESH = 240 # 4 minutes

    def __init__(self,server_url:str,username:str,password:str):
        self.username=username
        self.password=password
        self.session = requests.session()
        self.server_url = server_url + "/" if server_url[-1]!="/" else server_url
        headers={"Content-Type": "application/json"}
        data = {"username":username,"password":password}
        r = self.session.post(self.server_url+"api/token-auth/",json=data,headers=headers)
        self.access = r.json()["access"]
        self.refresh = r.json()["refresh"]
        self.last_request = datetime.now()

    def _refresh(self):
        headers="Content-Type: application/json"
        data = {"refresh":self.refresh}
        r = self.session.post(self.server_url+"api/token-refresh/",json=data,headers=headers)
        self.access = r.json()["access"]

    def request(self,headers: dict=None,
                method: str=None,
                path:str=None,
                json:dict=None,
                params:dict=None
                ):
        elapsed = datetime.now() - self.last_request
        if elapsed.seconds > JWT.REFRESH:
            self._refresh()
        headers_req = {} if headers is None else headers
        headers_req["Authorization"]="Bearer "+self.access
        r = self.session.request(method,
                                 json=json,
                                 url=self.server_url+path,
                                 params=params,
                                 headers=headers_req)
        return r.status_code,r.json()


class Hello(unittest.TestCase):
    def setUp(self):
        username = os.environ["username"]
        password = os.environ["password"]
        server_url = os.environ["server_url"]
        self.processor = JWT(server_url,username,password)



    def test_hello(self):
        status,data_dict = self.processor.request(method="GET",path="api/hello")
        content = {'message': 'Hello, World!'}
        self.assertEqual(data_dict,content)

    def test_unshorten(self):

        headers={"Content-Type": "application/json"}
        status,data_dict = self.processor.request(method="POST",headers=headers,
                                             path="api/urls/unshorten/",json={"url":"http://cnn.com"})
        self.assertEqual(201,status)
        self.assertEqual(data_dict["unshortened_url"],"https://www.cnn.com/")

    def test_rasterize_job(self):

        headers={"Content-Type": "application/json"}
        status,data_dict = self.processor.request(method="POST",headers=headers,
                                             path="api/urls/rasterize/",json={"url":"http://cnn.com"})
        self.assertEqual(201,status)




if __name__=="__main__":
    unittest.main()
