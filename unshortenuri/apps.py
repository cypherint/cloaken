from django.apps import AppConfig


class UnshortenuriConfig(AppConfig):
    name = 'unshortenuri'
