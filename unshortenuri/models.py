from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from django.utils import timezone
from background_task.models import Task
# Create your models here.

#users

class Url(models.Model):
    url_id = models.AutoField(primary_key=True)
    url = models.URLField(max_length=1000)
    unshortened_url= models.URLField(max_length=1000)
    date_time = models.DateTimeField(default=timezone.now,editable=False)
    user = models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return self.url_id + ": " + self.url + "; "


class RasterizeResult(models.Model):
    task_result_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    url = models.URLField(max_length=1000)
    page_source = models.TextField(blank=True)
    page_png = models.ImageField(null=True,blank=True)
    completed= models.BooleanField(default=False)
    task_hash = models.CharField(unique=True,max_length=40, db_index=True)

