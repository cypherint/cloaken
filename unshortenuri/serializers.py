from rest_framework import serializers
from django.contrib.auth.models import User
from background_task import models as bk_models
from background_task import models_completed as bk_completed_models
from .models import Url,RasterizeResult


class UUIDTaskSerializer(serializers.BaseSerializer):
    task_hash = None
    task_uuid = None


class URLSerializerDummy(serializers.BaseSerializer):
    url = serializers.CharField(max_length=1000)


class TaskStatusSerializer(serializers.BaseSerializer):
    task_status = serializers.CharField(max_length=50)
    task_hash = serializers.CharField(max_length=300)



class CompletedTaskSerializer(serializers.ModelSerializer):
    class Meta:
        fields=[
            'id',
            'task_name',
            'task_params',
            'task_hash',
            'priority',
            'run_at',
            'queue',
            'attempts',
            'failed_at',
            'last_error',
            'locked_by',
            'locked_at',
            'verbose_name',
            'creator',
            'repeat',
            'repeat_until',
        ]
        model = bk_completed_models.CompletedTask


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        fields=[
            'id',
            'task_name',
            'task_params',
            'task_hash',
            'verbose_name',
            'priority',
            'run_at',
            'repeat',
            'repeat_until',
            'queue',
            'attempts',
            'failed_at',
            'last_error',
            'locked_by',
            'creator',
            'creator_object_id',
            'creator_content_type'
        ]
        model = bk_models.Task


class RasterizeResultSerializer(serializers.ModelSerializer):
    class Meta:
        fields=[
            "task_result_id",
            "user",
            "url",
            "page_source",
            "page_png",
            "completed",
            "task_hash"
        ]
        model = RasterizeResult


class UrlSerializerOriginal(serializers.ModelSerializer):
    class Meta:
        fields=[
            'url',
        ]
        model = Url


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'username',
        ]
        read_only_fields=['username',]

class UserSerializerUpdate(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password')

    def create(self,*args,**kwargs):
        user = super().create(*args,**kwargs)
        p = user.password
        user.set_password(p)
        user.save()
        return user

    def update(self, *args,**kwargs):
        user = super().update(*args,**kwargs)
        p = user.password
        user.set_password(p)
        user.save()
        return user

class UrlSerializer(serializers.ModelSerializer):
    class Meta:
        fields=[
            'url_id',
            'url',
            'unshortened_url',
            'date_time',
            'user'
        ]
        model = Url


class UserSerializerWithToken(serializers.ModelSerializer):
    token = serializers.SerializerMethodField(method_name='get_token')

    class Meta:
        fields={
            'username',
            'password'

        }
        extra_kwargs={'password':{'write_only':True}}

    def get_token(self):
        pass
