import io
from datetime import datetime

class Comment(object):
    def __init__(self, email, content, created=None):
        self.email = email
        self.content = content
        self.created = created or datetime.now()


comment = Comment(email='leila@example.com', content='foo bar')
from rest_framework import serializers
from rest_framework.renderers import JSONRenderer


class CommentSerializer(serializers.Serializer):
    email = serializers.EmailField()
    content = serializers.CharField(max_length=200)
    created = serializers.DateTimeField()
    def create(self, validated_data):
        """
        return an object from validated data
        :param validated_data:
        :return:
        """
        return Comment(**validated_data)


    def update(self, instance, validated_data):
        """
        update an object from validated data
        :param instance:
        :param validated_data:
        :return:
        """
        instance.email = validated_data.get('email', instance.email)
        instance.content = validated_data.get('content', instance.content)
        instance.created = validated_data.get('created', instance.created)
        return instance

#object is serialized into a dictionary
serializer = CommentSerializer(comment)

#serializer.data contains the dict parsed from the object

#django then can render the json string
json_rendered_string = JSONRenderer().render(serializer.data)

#opposite read a stream into an object
import io
stream = io.BytesIO(json_rendered_string)

from rest_framework.parsers import JSONParser
#parse stream into a dict
dict_parsed = JSONParser().parse(stream)
#this time we set the data immediately since we already have a dict
serializer2 = CommentSerializer(data=dict_parsed)
#check that dict is valid
serializer2.is_valid()

#use serializer to return an object
comment_inst = serializer2.save()

#update an existing comment
serializer2 = CommentSerializer(comment_inst,data=dict_parsed)
serializer2.is_valid()
serializer2.save()


