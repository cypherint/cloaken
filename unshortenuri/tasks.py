from background_task import background
from selenium import webdriver
from django.conf import settings
from .models import RasterizeResult
from django.core.files import File
from io import BytesIO

def rasterize_bare(url=None,result_id=None,**kwargs):
    # Option 1 - with ChromeOptions
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--headless')
    if settings.PROXIES is not None:
        chrome_options.add_argument("--proxy-server=" + settings.PROXIES["http"])
    chrome_options.add_argument("--hide-scrollbars")
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument(
        '--no-sandbox')  # required when running as root user. otherwise you would get no sandbox errors.
    driver = webdriver.Chrome(executable_path=settings.CHROME_DRIVER, chrome_options=chrome_options,
                              service_args=['--verbose', '--log-path=/tmp/chromedriver.log'])
    # Option 2 - with pyvirtualdisplay
    #from pyvirtualdisplay import Display
    #display = Display(visible=0, size=(1024, 768))
    #display.start()
    #driver = webdriver.Chrome(driver_path=settings.CHROME_DRIVER,
    #                          service_args=['--verbose', '--log-path=/tmp/chromedriver.log'])
    # Log path added via service_args to see errors if something goes wrong (always a good idea - many of the errors I encountered were described in the logs)
    # And now you can add your website / app testing functionality:
    driver.get(url)
    page_source = driver.page_source
    png = driver.get_screenshot_as_png()
    output = BytesIO()
    output.write(png)
    # todo(aj) screenshot
    # update record with task_request_id , screenshot, and page_source
    record = RasterizeResult.objects.get(task_result_id=result_id)
    record.page_source=page_source
    record.completed=True
    record.page_png.save('screenshot.png',File(output))
    record.save()

@background(schedule=10)
def rasterize(url=None,result_id=None,**kwargs):
    """
    :param url: string - url to unshorten
    :param id: uuid - used to relate job to result
    :param kwargs:
    """
    rasterize_bare(url,result_id,**kwargs)

