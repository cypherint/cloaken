
# Create your tests here.
from unittest.mock import MagicMock
from unittest.mock import patch
from django.test import TestCase
from django.test import Client
from django.contrib.auth.models import User
from .models import RasterizeResult
from .tasks import rasterize_bare
from undecorated import undecorated



class JobTest(TestCase):
    def setUp(self):
        self.c = Client()
    @patch('unshortenuri.tasks.webdriver')
    def test_rasterize_job(self,patched_webdriver):
        patched_webdriver.Chrome.return_value.get.return_value = 'testing'
        patched_webdriver.Chrome.return_value.page_source = 'testing'
        patched_webdriver.Chrome.return_value.get_screenshot_as_png.return_value = b'testing'
        failed = False
        url = "http://cnn.com"
        user =User.objects.create_user(username="test",password="test",email="test@test.com")
        rasterize_result = RasterizeResult.objects.create(url=url,user=user)
        try:
            rasterize_bare(url=url,result_id=rasterize_result.task_result_id)
        except:
            failed=True
        self.assertTrue(patched_webdriver.Chrome.called_once)
        self.assertEqual(failed,False)
