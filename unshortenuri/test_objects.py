from django.test import TestCase
from unittest.mock import patch

# Create your tests here.
from unittest import mock
from django.test import TestCase
from django.test import Client
from .models import Url,User
from datetime import datetime




class ModelTest(TestCase):

    def test_title_content(self):
        user = User.objects.create_user("jim","jim@jim.com","jimmy1123")
        Url.objects.create(url="http://cnn.com",unshortened_url='http://cnn.com',user=user)
        url = Url.objects.get(url_id=1)
        expected_object_url = f'{url.url}'
        expected_object_unshortened = f'{url.unshortened_url}'
        self.assertEquals(expected_object_url, 'http://cnn.com')
        self.assertEquals(expected_object_unshortened,'http://cnn.com')


