"""
Cloaken server must be running to run tests below

set USERNAME environment variable
set PASSWORD environment variable
"""
# Create your tests here.
from unittest import mock
from django.test import TestCase
from django.test import Client
from .models import Url,User
from datetime import datetime
from rest_framework import status
import os

USERNAME = os.environ.get("USERNAME","unshorten")
PASSWORD = os.environ.get("PASSWORD","hinton50")


# This method will be used by the mock to replace requests.get
def mocked_requests_head(*args, **kwargs):
    class MockResponse:
        def __init__(self, url, status_code):
            self.url= url
            self.status_code = status_code
            self.json_data = {}
        def json(self):
            return self.json_data

    if args[0] == 'http://cnn.com':
        return MockResponse(args[0], 200)
    elif args[0] == 'http://bad':
        return MockResponse(args[0], 400)

TEST_HASH_RUNNING="RUNNING"
TEST_HASH_COMPLETED="Completed"


def mocked_not_found(*args,**kwargs):
    return []


def mocked_completed_filter(*args,**kwargs):
    if kwargs["task_hash"]==TEST_HASH_RUNNING:
        return []
    if kwargs["task_hash"] == TEST_HASH_COMPLETED:
        return[1]


def mocked_running_filter(*args,**kwargs):
    if kwargs["task_hash"]==TEST_HASH_RUNNING:
        return [1]
    if kwargs["task_hash"] == TEST_HASH_COMPLETED:
        return[]

class TestView(TestCase):
    # todo(aj) set fixture with username
    fixtures = ['initial.json']
    GET="GET"
    POST="POST"
    REFRESH = 240 # 4 minutes

    def setUp(self):
        self.c = Client()
        username=USERNAME
        password=PASSWORD
        headers={"Content-Type":"application/json"}
        data = {"username":username,"password":password}
        r = self.c.post("/api/token-auth/",data=data,headers=headers)
        self.access = r.json()["access"]
        self.refresh = r.json()["refresh"]
        self.last_request = datetime.now()
        self.c.login(username=username, password=password)

    def _refresh(self):
        headers="Content-Type: application/json"
        data = {"refresh":self.refresh}
        r = self.c.post("/api/token-refresh/",data=data,headers=headers)
        self.access = r.json()["access"]

    def request(self,headers: dict=None,
                method: str=None,
                path:str=None,
                data:dict=None,
                params:dict=None):
        elapsed = datetime.now() - self.last_request
        if elapsed.seconds > TestView.REFRESH:
            self._refresh()
        headers_req = {} if headers is None else headers
        headers_req["Authorization"]="Bearer "+self.access
        if method=="POST":
            r = self.c.post(path,
                            data=data,
                            headers=headers_req)
        elif method=="GET":
             r = self.c.get(path,
                            params=params,
                            headers=headers_req)
        else:
            raise NotImplemented("method " + method + " not implemented")
        return r.status_code,r.json()

    def test_set_password(self):
        json_data = {"username":"unshorten","password":"hinton501"}
        r = self.request(method="POST",path="/api/users/unshorten/set_password/",data=json_data)
        self.assertEqual(r[0],status.HTTP_200_OK)

    @mock.patch('unshortenuri.views.requests.head',side_effect=mocked_requests_head)
    def test_unshorten(self,patch_head):
        data = {"url":"http://cnn.com"}
        r = self.request(method="POST", path='/api/urls/unshorten/', data=data)
        self.assertEqual(r[0],status.HTTP_201_CREATED)

    @mock.patch('unshortenuri.views.requests.head',side_effect=mocked_requests_head)
    def test_unshorten_badrequest(self,patch_head):
        data = {"url":"http://bad"}
        r = self.request(method="POST", path='/api/urls/unshorten/', data=data)
        self.assertEqual(r[0],status.HTTP_400_BAD_REQUEST)

    def test_rasterize_task(self):
        data = {"url":"http://cnn.com"}
        r = self.request(method="POST",path='/api/urls/rasterize/',data=data)
        self.assertEqual(r[0],status.HTTP_201_CREATED)

    @mock.patch('unshortenuri.views.Task.objects.filter',side_effect=mocked_running_filter)
    @mock.patch('unshortenuri.views.CompletedTask.objects.filter',side_effect=mocked_completed_filter)
    def test_rasterize_status(self,patch_task_filter,patch_completed_filter):
        r = self.request(method="GET",path='/api/task_status/'+TEST_HASH_COMPLETED)
        self.assertEqual(r[0],status.HTTP_200_OK)
        self.assertEqual(r[1]["task_status"],"completed")

    @mock.patch('unshortenuri.views.Task.objects.filter',side_effect=mocked_completed_filter)
    @mock.patch('unshortenuri.views.CompletedTask.objects.filter',side_effect=mocked_running_filter)
    def test_rasterize_status(self,patch_task_filter,patch_completed_filter):
        r = self.request(method="GET",path='/api/task_status/'+TEST_HASH_COMPLETED)
        self.assertEqual(r[0],status.HTTP_200_OK)
        self.assertEqual(r[1]["task_status"],"running")

    @mock.patch('unshortenuri.views.Task.objects.filter',side_effect=mocked_not_found)
    @mock.patch('unshortenuri.views.CompletedTask.objects.filter',side_effect=mocked_not_found)
    def test_rasterize_status(self,patch_task_filter,patch_completed_filter):
        r = self.request(method="GET",path='/api/task_status/'+TEST_HASH_COMPLETED)
        self.assertEqual(r[0],status.HTTP_400_BAD_REQUEST)
        self.assertEqual(r[1]["task_status"],"error - not found")



