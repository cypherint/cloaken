
from django.urls import path,include
from django.conf.urls import url
from . import views as local_views
from rest_framework.schemas import get_schema_view
from rest_framework import permissions

from drf_yasg.views import get_schema_view as get_schema_yasg
from drf_yasg import openapi


yasg_schema_view = get_schema_yasg(
   openapi.Info(
      title="Cloaken V1",
      default_version='v1',
      description="Cloaken",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="aaron.jonen@cypherint.com"),
      license=openapi.License(name="Private License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)


from rest_framework import routers
from rest_framework_simplejwt.views import TokenObtainPairView,TokenRefreshView,TokenVerifyView
router = routers.DefaultRouter()
router.register('urls',local_views.UrlViewSet)
router.register('users',local_views.UserViewSet)
router.register('rasterizeresults',local_views.RasterizeResultsViewSet)
router.register('tasks',local_views.TaskViewSet)
router.register('completed-tasks',local_views.CompletedTaskViewSet)
#router.register('urls/rasterize',local_views.Rasterize,base_name="urls_rasterize")


urlpatterns =[
    path('urls/unshorten/',local_views.Unshorten.as_view()),
    path('urls/rasterize/',local_views.Rasterize.as_view()),
    path('',include(router.urls)),
    path('token-auth/', TokenObtainPairView.as_view()),
    path('token-refresh/', TokenRefreshView.as_view()),
    path('token-verify/', TokenVerifyView.as_view()),
    path('hello/', local_views.HelloView.as_view()),
    path('auth',include('rest_framework.urls')),
    path('schema/',get_schema_view(title="Cloaken API")),
    path('task_status/<str:task_hash>',local_views.TaskHashStatus.as_view()),
    url(r'^swagger(?P<format>\.json|\.yaml)$', yasg_schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('swagger/', yasg_schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', yasg_schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]

