from django.shortcuts import render
from .models import RasterizeResult
from .tasks import rasterize
from functools import partial
import coreschema
import uuid
import coreapi
from rest_framework import generics,viewsets
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView,GenericAPIView
from rest_framework.response import Response
from django.conf import settings
from django.contrib.auth.models import User
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework.decorators import action,api_view
from rest_framework import permissions
from rest_framework import filters as rest_filters
from django_filters import rest_framework as filters
import requests
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from .models import Url
from .serializers import UrlSerializer,UserSerializer,UrlSerializerOriginal,UserSerializerUpdate,TaskSerializer
from .serializers import RasterizeResultSerializer,CompletedTaskSerializer,TaskStatusSerializer,URLSerializerDummy


class IsSelf(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.username==request.user


class IsUserOrAdmin(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.username ==request.user or request.user.is_admin


class UserFilter(filters.FilterSet):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'is_active', 'date_joined')


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filterset_fields = ('username', 'first_name', 'last_name', 'email', 'is_active', 'date_joined')
    filterset_class = UserFilter

    @action(detail=True,methods=['post'], permission_classes=[IsUserOrAdmin])
    def set_password(self, request, pk=None):
        user = request.user
        serializer=UserSerializerUpdate(user,data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'status':'password set'},status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)


class HelloView(APIView):

    def get(self, request):
        content = {'message': 'Hello, World!'}
        return Response(content)


class RasterizeFilter(filters.FilterSet):
    class Meta:
        model=RasterizeResult
        fields = ('task_result_id','user','url','completed','task_hash',)


class RasterizeResultsViewSet(viewsets.ModelViewSet):
    queryset = RasterizeResult.objects.all()
    serializer_class = RasterizeResultSerializer
    fields = ('task_result_id','user','url','completed','task',)
    filterset_class=RasterizeFilter


    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)



class UrlFilter(filters.FilterSet):
    class Meta:
        model=Url
        fields = ("url","unshortened_url")


class UrlViewSet(viewsets.ModelViewSet):
    queryset = Url.objects.all().order_by('url_id')
    serializer_class = UrlSerializer
    filterset_class=UrlFilter
    fields = ("url","unshortened_url")

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)

from background_task.models import Task
from background_task.models_completed import CompletedTask
from datetime import datetime
from django.utils import timezone

def get_status(request):
    now = timezone.now()

    # pending tasks will have `run_at` column greater than current time.
    # Similar for running tasks, it shall be
    # greater than or equal to `locked_at` column.
    # Running tasks won't work with SQLite DB,
    # because of concurrency issues in SQLite.
    pending_tasks_qs = Task.objects.filter(run_at__gt=now)
    running_tasks_qs = Task.objects.filter(locked_at__gte=now)

    # Completed tasks goes in `CompletedTask` model.
    # I have picked all, you can choose to filter based on what you want.
    completed_tasks_qs = CompletedTask.objects.all()
    # main logic here to return this as a response.
    # just for test
    print(pending_tasks_qs, running_tasks_qs, completed_tasks_qs)


class Rasterize(APIView):
    # todo create dummy serializer class

    #url = openapi.Parameter('url',
    #                        in_=openapi.IN_BODY,
    #                        required=True,
    #                        description="URL to unshorten",
    #                        type=openapi.TYPE_STRING)
    response = openapi.Response('Hash for polling job',RasterizeResultSerializer)
    @swagger_auto_schema(responses={201: response},
                         request_body=openapi.Schema(
                             type=openapi.TYPE_OBJECT,
                             required=['url'],
                             properties={
                                'url': openapi.Schema(type=openapi.TYPE_STRING)
                             }))

    def post(self, request):
        """
        400 error returned for urls that are missing or malformed
        201 error returned for success
        :param request:
        :return: Reponse
        """
        org_url = self.request.data.get("url",None)
        if org_url is None:
            return Response({ "error":"missing url parameter" },status=status.HTTP_400_BAD_REQUEST)
        # create object to get id from rasterize model.
        rasterize_result = RasterizeResult.objects.create(url=org_url,user=self.request.user)

        #create job
        rasterize_task = rasterize(url=org_url,result_id=rasterize_result.task_result_id)
        rasterize_task.save()
        # set update rasterize model task id
        rasterize_result.task_hash=rasterize_task.task_hash
        rasterize_result.save()

        # serializer = TaskSerializer(rasterize_task)
        serializer_result = RasterizeResultSerializer(rasterize_result)
        return Response(serializer_result.data,status=status.HTTP_201_CREATED)


class TaskHashStatus(APIView):
    #serializer_class = TaskStatusSerializer

    #def get_serializer_class(self):
    #    return self.serializer_class
    #url = openapi.Parameter('url',
    #                        in_=openapi.IN_BODY,
    #                        required=True,
    #                        description="URL to unshorten",
    #                        type=openapi.TYPE_STRING)

    response = openapi.Response('Hash status',openapi.Schema(
                             type=openapi.TYPE_OBJECT,
                             properties={
                                'task_status': openapi.Schema(type=openapi.TYPE_STRING),
                                'task_hash': openapi.Schema(type=openapi.TYPE_STRING)
                             }))

    @swagger_auto_schema(responses={200: response})
    def get(self,request,task_hash):
        running_status = Task.objects.filter(task_hash=task_hash)
        completed_status = CompletedTask.objects.filter(task_hash=task_hash)
        if len(completed_status) > 0:
            data={ "task_status":"completed", "task_hash":task_hash}
            return Response(data, status=status.HTTP_200_OK)
        elif len(completed_status) == 0 and len(running_status) > 0:
            data={ "task_status":"running", "task_hash":task_hash}
            return Response(data, status=status.HTTP_200_OK)
        else:
            data={"task_status":"error - not found", "task_hash":task_hash}
            return Response(data, status=status.HTTP_400_BAD_REQUEST)


class TaskFilter(filters.FilterSet):
    class Meta:
        model=Task
        fields = ("id","task_hash")

class TaskViewSet(viewsets.ModelViewSet):
    serializer_class = TaskSerializer
    queryset = Task.objects.all()
    filterset_class=TaskFilter
    fields = ('id',
            'task_name',
            'task_params',
            'task_hash',
            'verbose_name',
            'priority',
            'run_at',
            'repeat',
            'repeat_until',
            'queue',
            'attempts',
            'failed_at',
            'last_error',
            'locked_by',
            'creator',
            'creator_object_id',
            'creator_content_type')


class CompletedTaskFilter(filters.FilterSet):
    class Meta:
        model=CompletedTask
        fields = ("id","task_hash")


class CompletedTaskViewSet(viewsets.ModelViewSet):
    serializer_class = CompletedTaskSerializer
    queryset = CompletedTask.objects.all()
    filterset_class=CompletedTaskFilter
    fields = (
             'id',
            'task_name',
            'task_params',
            'task_hash',
            'priority',
            'run_at',
            'queue',
            'attempts',
            'failed_at',
            'last_error',
            'locked_by',
            'locked_at',
            'verbose_name',
            'creator',
            'repeat',
            'repeat_until',
    )


class Unshorten(APIView):
    response = openapi.Response('Hash for polling job',UrlSerializer)
    @swagger_auto_schema(responses={201: response},
                         request_body=openapi.Schema(
                             type=openapi.TYPE_OBJECT,
                             required=['url'],
                             properties={
                                'url': openapi.Schema(type=openapi.TYPE_STRING)
                             }))

    def post(self, request):
        """
        400 error returned for urls that are missing or malformed
        201 error returned for success
        :param request:
        :return: Reponse
        """
        org_url = self.request.data.get("url", None)
        if org_url is None:
            return Response({ "url":"missing url parameter"}, status=status.HTTP_400_BAD_REQUEST)
        try:
            r = requests.head(org_url, allow_redirects=True, proxies=settings.PROXIES)
        except requests.exceptions.RequestException as e:
            return Response({"url":str(e)}, status=status.HTTP_400_BAD_REQUEST)
        url_json = {"url":r.url}
        entity_serializer = UrlSerializerOriginal(data=url_json)
        if not entity_serializer.is_valid():
            return Response(entity_serializer.error_messages, status=status.HTTP_400_BAD_REQUEST)
        unshortened_url = r.url
        entry = Url(url=org_url, unshortened_url=unshortened_url, user=self.request.user)
        entry.save()
        serializer = UrlSerializer(entry)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
