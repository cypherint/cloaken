#!/usr/bin/env bash
set -e
cd ..
pipenv run python manage.py generate_secret_key --replace
pipenv run python manage.py createsuperuser
echo "Enter your registered DNS name: "
read dns_name
sudo systemctl restart postgresql
password=$(trap - PIPE ; cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
sudo sed -i -e "s/hinton50/$password/" ./siteapi/settings.py
sudo -u postgres psql -U postgres -d postgres -c "alter user unshorten with password '$password';"
sudo sed -i -e "s/server_name.*;/server_name $dns_name;/g" /etc/nginx/sites-available/default
sudo systemctl restart nginx
sudo systemctl restart gunicorn
echo "running certbot for ssl; please enter 2 to redirect port 80 to 443"
sudo certbot --nginx -d $dns_name
sudo certbot renew --dry-run

